public class Application
{
    public static void main(String[] args)
    {
        Cat cat1 = new Cat();
        cat1.breed = "Yellow";
        cat1.age = 5;
        cat1.sex = "Male";

        Cat cat2 = new Cat();
        cat2.breed = "Green";
        cat2.age = 6;
        cat2.sex = "Female";
		
		Cat[] cats = new Cat[3];
		cats[0] = cat1;
		cats[1] = cat2;
        cats[2] = new Cat();

        cats[2].breed = "Red";
        cats[2].age = 8;
        cats[2].sex = "Female";

        System.out.println(cat1.sex);
        System.out.println(cat1.breed);
        System.out.println(cat1.age);

        System.out.println(cats[2].sex);
        System.out.println(cats[2].breed);
        System.out.println(cats[2].age);

        System.out.println(cats[2].increaseAge());
        cats[0].hasStripes();


    }
}